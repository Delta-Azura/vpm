! /bin/bash

function build() {
    lock
    $PKG=/usr/share/vpm/$1/pkg
    mkdir $PKG
    echo "We are building $1" 
    cd /usr/share/vpm/$1
    chmod u+x Pkgfile
    source Pkgfile
    echo $source
    cd /usr/bin/
    find $depends* >/dev/null 2>&1
    if [[ $? != "0" ]];then
        error=$(echo "Need some packages to build : $depends")
        log $error
        unlock
        exit
    fi
    makedepends=$(cat makedepends)
    find $makedepends 
    if [[ $? != "0" ]];then
        error=$(echo "Need some makedepends : $makedepends")
        log $error
        unlock
        exit
    fi
    mkdir -p /usr/share/vpm/$1/cache
    cd /usr/share/vpm/$1/cache
    wget -c $source
    archive=$(ls)
    case ${archive} in 
        *.zip)
            unzip $archive
        ;;
        *.tar.gz)
            tar -xf $archive
        ;;
        *.tar)
            tar –xvf $archive
        ;;
        *.tar.bz2)
            tar -xvjf $archive
        ;;
        *tar.xz)
            tar -xvf $archive
    esac
    if [[ $? != "0" ]];then
        echo "error"
        exit
    fi 
    rm -rf $archive
    name_sources=$(ls)
    ls
    build
    if [[ $? != "0" ]];then
        error=$(echo "error")
        log $error
        unlock
        exit
    fi
    ./postinstall 
    if [[ $? != "0" ]];then 
        echo "No postinstall needed"
    fi
    cd /usr/share/vpm/$1/pkg
    echo $name > info
    echo "$version" >> info
    echo "$release" >> info
    echo "$packager" >> info
    echo "$depends" >> info
    echo "$makedepends" >> info
    files=$(find -type f -name "*")
    filesnb=$(echo $files | wc -w)
    echo $files
    sudo rm -rf files 
    for z in `seq 1 $filesnb`;
    do
        file=$(echo $files | cut -d " " -f$z)
        str=$file
        echo ${str/.\//\/} >> files
    done 
    tar -cJf $1.azura.tar.xz *
    read -p "Install $1 ? : [y/n] " to_install
    if [[ $to_install == "y" ]];then
        install $1.azura.tar.xz
    else 
        echo "ok"
    fi 
    read -p "Do you want to remove the work ? : [y/n] " vpmbuild
    if [[ $vpmbuild != "y" ]];then
        echo "ok"
    else 
        rm -rf /usr/share/vpm/$1
    fi 
    unlock
    echo "Done"
}

function install() {
    if [[ -f /var/cache/vpm/lock ]];then
        echo "already locked"
    else 
        lock
    fi
    tar -xf $1
    dir=$(ls -d */ | wc -w)
    for i in `seq 1 $dir`;
    do 
        bin=$(ls -d */ | cut -d ' ' -f$i)
        cp -r $bin/* /$bin/
    done
    echo "Work in progress"
    name_package=$(echo $1 | cut -d "." -f1)
    mkdir /var/lib/pkg/DB/$name_package
    if [[ $? != "0" ]];then
        rm -rf /var/lib/pkg/DB/$name_package
        mkdir /var/lib/pkg/DB/$name_package
    fi 
    cd /usr/share/vpm/$name_package/pkg
    cp files /var/lib/pkg/DB/$name_package/
    cp info /var/lib/pkg/DB/$name_package/
    read -p "Want to keep the archive ? [y/n] : " $rep
    if [[ $rep == "y" ]];then
        rm -rf $1.tar.xz
    fi
    unlock 
}

function get() {
    lock
    nbpkg=$(echo $@ | wc -w)
    echo $nbpkg
    for z in `seq 1 $nbpkg`;
    do
        deps=""
        pkg=$(echo $@ | cut -d " " -f$z)
        wget $repo/$pkg.tar.xz
        #unzip
        #install
        #read deps
        deps=$depends 
        depsi="$depends $deps"
    done
    nbdp=$(echo $depsi | wc -w)
    while [[ $deps != "" ]];
    do
        $deps="" 
        for y in `seq 1 $depsi`;
        do 
        idep=$(echo $depsi | cut -d " " -f$y)
        wget $repo/$idep.tar.xz
        install $idep.tar.xz
        source /var/lib/pkg/DB/$idep/files
        depss=$(echo $deps)
        idep=""
        idep=$($depends $depss) 
        done
    done 
    unlock
}

function update() {
    lock
    cd /usr/local/vpm/
    packages=$(ls)
    release=$(cat */info/release)
    echo "$release" | tee -a /usr/local/vpm/update >/dev/null 2>&1
    cd /home/$USER/.cache/
    rm -rf vpmbuild
    git clone https://gitlab.com/Delta-Azura/vpmbuild.git
    cd vpmbuild/src
    diff=$(cat */release)
    if [[ $release != $diff ]];then
        echo "Il n'y a aucun paquet à mettre à jour"
    else 
        cp -r $packages*/ /usr/share/vpm/
        echo "commming soon"
    fi
    unlock 
}

function info() {
    cat /var/lib/pkg/DB/$1/info
    #source info
    #echo "Name : $name"
    #echo "Description : $description"
    #echo "Release : $release"
    #echo "Sources : $sources"
    #echo "Packager : $packager"
    #rm -rf $1.azura.tar.xz
    #tar -cJf $1.azura.tar.xz *
}

function files() {
    cat /var/lib/pkg/DB/$1/files
}

function remove() {
    lock
    remove=$(cat /var/lib/pkg/DB/$1/files)
    sudo rm -rf $remove 
    unlock
}

function history() {
    cat /var/log/vpm.log 
}

function share() {
    echo "lol"
}

function log() { 
    date=$(date)
    if [[ -f "/var/log/vpm.log" ]];then
    	echo "$1 $2 $date" | tee -a /var/log/vpm.log
    else 
        touch /var/log/vpm.log
        echo "$1 $2 $date" | tee -a /var/log/vpm.log
    fi
    if [[ $2 == "error" ]];then
        echo "Error on : $1 $2 $date" | tee -a /var/log/vpm.log
    fi
}


function lock() {
    if [[ -d "/var/cache/vpm" ]];then
        touch /var/cache/vpm/lock
    else 
        mkdir /var/cache/vpm
        touch /var/cache/vpm/lock
        if [[ $? != "0" ]];then
            echo "vpm is active"
            exit
        fi
    fi 
}

function unlock() {
    rm -rf /var/cache/vpm/lock 
}

function main() {
    cd /etc/
    source akc.conf 
    log $1 $2 >/dev/null 2>&1
    if [[ -f "/etc/vpm/conf" ]];then
        echo "All configurations settings are on"
    else 
        mkdir -p /usr/local/vpm/
        mkdir -p /etc/vpm
        touch /etc/vpm/conf
    fi
    if [[ $1 == "get" ]];then
        get ${*:2}
    fi
    
    if [[ $1 == "build" ]];then
        build $2
    fi 

    if [[ $1 == "install" ]];then
        install $2
    fi
    if [[ $1 == "info" ]];then
        info $2
    fi
    if [[ $1 == "remove" ]];then
        remove $2
    fi
    if [[ $1 == "history" ]];then
        history
    fi 
    if [[ $1 == "share" ]];then
        share $2
    fi 

    if [[ $1 == "files" ]];then
        files $2
    fi
}
main "$@"
