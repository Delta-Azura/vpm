# What is vpm ? 
Vpm is a lightweight a simple package manager made from scratch !

# How to build and install it ? 
Easy, need git wget and bash at depends and shc at makedepends

```bash
$ git clone https://gitlab.com/Delta-Azura/vpm.git
```

```bash
$ sudo cp -r /home/$USER/vpm/vpm /usr/share/vpm/
``` 
```bash
$ cd ~/vpm/src/ && sudo ./main.sh build vpm
```
Then, vpm is installed on your system

If you need to install a tag (release) (not available now), then change sources file with correspondant link !

## How to use it ? 

# Need to build a package, then :

```bash
$ sudo mkdir -p /usr/share/vpm/your_package/info
$ cd /usr/share/vpm/ 
$ # Write the build instructions at bash script format
$ # Write the postinstall instructions at bash script format (optional)
$ cd info
$ sudo nano packager # Enter your name
$ sudo nano depends # Enter the depends of your package
$ sudo nano makedepends # Enter build depends of your package
$ sudo nano release # Enter the package release version
``` 
Now you are ready to launch this command :
```bash
$ sudo vpm build yourpackage
``` 
# Need information of your package ? 
```bash
$ sudo vpm info package
``` 

# Finally, maybe you want to look at vpm logs ? 
```bash
$ sudo vpm history
```

## VPM
Vpm is a personnal project, there is no garanty, it's just a passion 
